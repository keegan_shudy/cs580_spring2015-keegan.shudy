DUE: TUESDAY APRIL 28 2015

Write a senior thesis proposal that is related to the topic of computer graphics
and information visualization. 


Proposal must be:
	five pages long
	use the given template
	.bib reference file
	Title
	One paragraph abstract
	One technical diagram (TikZ?)
	one element such as a formal statement of an algorithm or equation or screen shot
	Be read and edited by by one other person
	

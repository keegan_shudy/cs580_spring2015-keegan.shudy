\documentclass[final,t]{beamer}
\mode<presentation>{ \usetheme{Major} }
  \usepackage{times}
  \usepackage{amsmath,amsthm, amssymb, latexsym}
  \boldmath
  \usepackage[english]{babel}
  
  	\usepackage{relsize}
  	\usepackage{multirow}
	\usepackage{qtree}
	\usepackage{stmaryrd}
	\usepackage{booktabs}
%  \usepackage[font=small,format=plain,labelfont=bf,up,textfont=it,up]{caption}
  	\usepackage[font=scriptsize,labelfont=scriptsize,bf]{caption}
  	\usepackage[latin1]{inputenc}
  	\usepackage[orientation=landscape,size=a1,scale=1.4,debug]{beamerposter}
  	\usepackage{color,listings}
  	\usepackage{calc,xcolor}
  	
\definecolor{lightblue}{rgb}{.85,.85,1} % 217 217 255
\definecolor{lightorange}{rgb}{1,.8,.6} % 255 204 153
\definecolor{lightgreen}{rgb}{.77,.91,.5} % 196 232 128
\definecolor{lightyellow}{rgb}{1,.98,.6} % 255 250 153
  	
  	
\newsavebox\CBox
\newenvironment{ColorBox}[3][black]{%
\par\noindent
\def\borderColor{#1}\def\bgColor{#2}
\begin{lrbox}{\CBox}
\minipage{#3-2\fboxsep-2\fboxrule}%
}{%
\endminipage\end{lrbox}%
\fcolorbox{\borderColor}{\bgColor}{\usebox\CBox}\par}
  	
\lstset{language=java}
\lstset{breaklines=true}
\lstset{showstringspaces=false}
\lstset{tabsize=3}
\lstset{basicstyle=\ttfamily\scriptsize}
\lstset{breakautoindent=true}
\lstset{postbreak=\space}
%\lstset{commentstyle=\color{XcodeComments}}
%\lstset{keywordstyle=\color{XcodeKeywords}}
%\lstset{stringstyle=\color{XcodeStringstyle}}

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
  \title[MAJOR]{MAJOR: An Efficient and Extensible Tool for\\\vspace*{2mm} \underline{M}utation \underline{A}nalysis in a \underline{J}ava C\underline{o}mpile\underline{r}}
  \author[Just \& Kapfhammer]{Ren\'e Just\textsuperscript{1} and Franz Schweiggert\textsuperscript{1} and Gregory M. Kapfhammer\textsuperscript{2}}
  \institute{\textsuperscript{1}Department of Applied Information Processing, Ulm University\\\textsuperscript{2}Department of Computer Science, Allegheny College}
  \date[ASE 2011] {26th IEEE/ACM International Conference on Automated Software Engineering (ASE 2011)}
  \webpage{rene.just@uni-ulm.de}
  \mail{gkapfham@allegheny.edu}

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
\begin{document}
  \begin{frame}{}  
  \vspace*{-6mm}
  	\begin{columns}[t]

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%      
%
% Left column
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  
      \begin{column}{.325\linewidth}
      
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		%
		% Important Contributions
		%
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        \begin{alertblock}{\textsc{Important Contributions}}
			\begin{itemize}
				\item Enhanced the Java 6 Standard Edition compiler
		
				\item Provides its own domain specific language (DSL)
		
				\item Easily applicable in all Java development environments
			
				\item Effectively reduces mutant generation time to a minimum
			\end{itemize}        
        \end{alertblock}        

		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		%
		% Conditional Mutation
		%
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        \begin{block}{\textsc{Conditional Mutation}}
        	\begin{itemize}
        		\item Transforms the program's abstract syntax tree (AST)

        		\item Encapsulates the mutations within conditional statements
        	\end{itemize}
			\begin{figure}			
				\begin{columns}[c]
    	  			\begin{column}{.25\linewidth}
  						\centering\includegraphics[width=.8\linewidth]{content/tree1}			
			      	\end{column}
					$\Rightarrow$
					\begin{column}{.65\linewidth}
						\centering\includegraphics[width=.8\linewidth]{content/tree2}
			      	\end{column}
				\end{columns}
				\caption{Multiple mutated binary expression as
                        the right hand side of an assignment statement.}
                \vspace*{-9mm}
			\end{figure}
        \end{block}

		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		%
		% Supported Features
		%
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		\begin{block}{\textsc{Supported Features}}
			\begin{itemize}
       			\item Simple compiler options enable the mutation analysis
       			
       			\item Configurable mutation operators by means of a DSL

       			\item Determination of mutation coverage by running the original code
      		\end{itemize}
   		\end{block}

		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		%
		% Mutation Coverage
		%
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   		\begin{block}{\textsc{Mutation Coverage}}
       		\begin{columns}[t]
   			\begin{column}{.43\linewidth}
   			\vspace*{-10mm}
				\begin{figure}				
				\input{content/listingCover}
				\caption{Collecting coverage information.}
				\vspace*{3mm}
				\end{figure}
			\end{column}
			\hspace*{-2cm}
   			\begin{column}{.58\linewidth}
   			\vspace*{2mm}
				\begin{itemize}
					\item It is impossible to kill a mutant if it is not reached and executed

					\item Additional instrumentation determines the covered mutations

					\item Mutation coverage is only examined if the tests execute the original code

					\item An external driver efficiently records the covered mutations as ranges

					\item Only those mutants covered by a test case are executed
				\end{itemize}
			\end{column}
			\end{columns}
		\end{block}
      \end{column}   
      
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%      
%
% Center column
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      \begin{column}{.325\linewidth}

   		
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		%
		% Implementation Details
		%
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		\begin{block}{\textsc{Implementation Details}}
			\begin{figure}
				\centering\includegraphics[width=.95\linewidth]{content/impl2}
				\caption{Integration of the conditional mutation 
						 approach into the compilation process.}
			\end{figure}
			\vspace*{-32mm}
			\begin{columns}[t]
			\begin{column}{.94\linewidth}
				\begin{columns}[t]
				\begin{column}{.475\linewidth}
					\begin{figure}
						\begin{ColorBox}{lightgreen}{\linewidth}
							\vspace*{-2mm}
							\include{content/listingDsl}
							\vspace*{-7mm}
						\end{ColorBox}
						\caption{DSL script to define the mutation process.}
					\end{figure}
				\end{column}
				\hspace*{1mm}
				\begin{column}{.475\linewidth}
					\begin{figure}
						\begin{ColorBox}{lightorange}{\linewidth}
							\vspace*{-2mm}
							\include{content/listingDriver}
							\vspace*{-7mm}
						\end{ColorBox}
						\caption{Simple driver class implementation.}
					\end{figure}			
				\end{column}
				\end{columns}
			\end{column}
			\end{columns}
		\end{block}
		
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		%
		% Optimized Workflow
		%
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%		
		\begin{block}{\textsc{Optimized Mutation Analysis Process}}
			\begin{figure}
				\centering\includegraphics[width=.9\linewidth]{content/workflow}
				\caption{Minimizing the runtime of mutation analysis by means of test prioritization and mutation coverage.}
			\end{figure}
			\vspace*{-4mm}
			\begin{enumerate}
				\item Embed and compile all mutants into the original program
				\item Run tests on original program to gather runtime and coverage
				\item Sort tests in ascending order according to their runtime
				\item Perform mutation analysis with reordered tests employing coverage information
			\end{enumerate}
		\end{block}
	\end{column}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%      
%
% Right column
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%      		
	\begin{column}{.325\linewidth}	
		
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		%
		% Runtime of MAJOR's Compiler
		%
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        \begin{block}{\textsc{Runtime of MAJOR's Compiler}}
			\begin{figure}
				\centering\includegraphics[width=.98\linewidth]{content/runtimeCompiler}
				\caption{Compiler runtime to generate
                                                  and compile the
                                                  mutants for all of
                                                  the projects.}
			\end{figure}
			\vspace*{-6mm}
			\begin{itemize}
				\item Negligible overhead for generating and
                          compiling the mutants
	
				\item Applicable on commodity workstations, even for large projects
                        
			\end{itemize}		
		\end{block}	    
	
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		%
		% Evaluation of Mutation Analysis Process
		%
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  
		\begin{block}{\textsc{Evaluation of Mutation Analysis Processes}}
			\begin{figure}
            	\centering\includegraphics[width=.98\linewidth]{content/runtimeWorkflow}
				\caption{Runtime of the mutation analysis processes.}
			\end{figure}
			\vspace*{-5mm}
		\end{block}
				
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		%
		% Future Work
		%
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%				
		\begin{alertblock}{\textsc{Future Work}}
        	\begin{itemize}
				\item Implementing new mutation operators at the semantic level
				\item Extending the domain specific language to support new operators
				\item Integrating conditional mutation into the new Java 7 compiler
			\end{itemize}
		\end{alertblock}
		\end{column}
	\end{columns}
  \end{frame}
\end{document}
